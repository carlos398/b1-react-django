from django.urls import path

from .views import *

urlpatterns = [
    path('', BlogListViewAPI.as_view()),
    path('<post_slug>/', PostDetailViewAPI.as_view())
]