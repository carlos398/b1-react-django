from django.shortcuts import get_object_or_404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Post
from .pagination import (LargeSetPagination, MediumSetPagination,
                         SmallSetPagination)
from .serializers import PostSerializer


class BlogListViewAPI(APIView):
    def get(self, request, fromat=None):
        if Post.postobjects.all().exists():

            posts = Post.postobjects.all()
            pagination = SmallSetPagination()
            results = pagination.paginate_queryset(posts, request)
            serializer = PostSerializer(results, many=True)

            return pagination.get_paginated_response({
                'posts': serializer.data
            })
        else:
            return Response({
                'error': 'No posts found'}, 
                status=status.HTTP_404_NOT_FOUND
            )


class PostDetailViewAPI(APIView):
    def get(self, request, post_slug, format=None):
        post = get_object_or_404(Post, slug=post_slug)
        serializer = PostSerializer(post)
        return Response({
            'post': serializer.data
        }, 
            status=status.HTTP_200_OK
        )
